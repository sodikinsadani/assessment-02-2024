package application

import (
	"fmt"
	"transaksi/data"

	"github.com/gofiber/fiber/v2"
	"github.com/sirupsen/logrus"
	"gorm.io/gorm"
)

func (a *TransaksiApplication) Authentication(ctx *fiber.Ctx, params data.Authentication) (err error) {
	tx, err := a.datastore.Begin()
	if err != nil {
		a.log.Error(logrus.Fields{
			"err": err,
		}, nil, "log error")
		a.datastore.Rollback(tx)
		return
	}

	encPin, err := a.Encrypt(params.Pin)
	if err != nil {
		a.log.Error(logrus.Fields{
			"err": err.Error(),
		}, nil, err.Error())
		a.datastore.Rollback(tx)
		return
	}

	params.Pin = encPin

	err = a.datastore.GetAuthentication(tx, params)
	if err != nil && err != gorm.ErrRecordNotFound {
		a.log.Error(logrus.Fields{
			"err": err,
		}, nil, "log error")
		a.datastore.Rollback(tx)

		return
	}

	if err == gorm.ErrRecordNotFound {
		a.log.Error(logrus.Fields{
			"err": err,
		}, nil, "log error")
		a.datastore.Rollback(tx)

		err = fmt.Errorf("autentikasi account_no dan pin tidak valid")

		return
	}

	a.datastore.Commit(tx)

	return
}
