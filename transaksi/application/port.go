package application

import (
	"transaksi/data"

	"github.com/gofiber/fiber/v2"
	"gorm.io/gorm"
)

type TransaksiAppPort interface {
	Authentication(ctx *fiber.Ctx, params data.Authentication) (err error)
	CreateTabung(ctx *fiber.Ctx, params data.CreateTabungSchema) (ResponseSchema data.ResponseSchema, TabungSchema data.CreateTabungAssessmentSchema, err error)
	CreateTarik(ctx *fiber.Ctx, params data.CreateTarikSchema) (ResponseSchema data.ResponseSchema, TarikSchema data.CreateTarikAssessmentSchema, err error)
}

type TransaksiDatastorePort interface {
	Begin() (tx *gorm.DB, err error)
	Rollback(tx *gorm.DB)
	Commit(tx *gorm.DB)

	GetAuthentication(tx *gorm.DB, params data.Authentication) (err error)

	GetRekening(tx *gorm.DB, nomorRekening string) (result data.Rekeningassessments, err error)
}
