package application

import (
	"encoding/json"
	"fmt"
	"time"
	"transaksi/data"

	"github.com/gofiber/fiber/v2"
	"github.com/sirupsen/logrus"
)

func (a *TransaksiApplication) CreateTabung(ctx *fiber.Ctx, params data.CreateTabungSchema) (ResponseSchema data.ResponseSchema, TabungSchema data.CreateTabungAssessmentSchema, err error) {
	tx, err := a.datastore.Begin()
	if err != nil {
		a.log.Error(logrus.Fields{
			"err":    err,
			"params": params,
		}, nil, "log error")
		a.datastore.Rollback(tx)
		ResponseSchema.RespCode = "02"
		ResponseSchema.Remak = err.Error()
		return
	}

	DataRekening, err := a.datastore.GetRekening(tx, params.NoRekening)
	if err != nil {
		a.log.Error(logrus.Fields{
			"err":    err,
			"params": params.NoRekening,
			"result": fmt.Sprintf("%+v", DataRekening),
		}, nil, "log error")
		a.datastore.Rollback(tx)
		ResponseSchema.RespCode = "02"
		ResponseSchema.Remak = err.Error()
		return
	}

	TanggalTransaksi := time.Now().Format("2006-01-02 15:04:05")
	var dataMutasi = data.CreateMutasiSchema{
		TanggalTransaksi: fmt.Sprintf("%v", TanggalTransaksi),
		NoRekening:       params.NoRekening,
		JenisTransaksi:   "C",
		Nominal:          params.Nominal,
	}
	jsonData, err := json.Marshal(dataMutasi)
	if err != nil {
		a.log.Error(logrus.Fields{
			"err":    err,
			"params": params,
		}, nil, "log error")
		a.datastore.Rollback(tx)
		ResponseSchema.RespCode = "02"
		ResponseSchema.Remak = err.Error()
		return
	}

	jsonString := string(jsonData)

	err = a.rdb.Publish(a.ctxRedis, "mutasi-transaksi", jsonString).Err()
	if err != nil {
		a.log.Error(logrus.Fields{
			"err":    err,
			"params": params,
		}, nil, "log error")
		a.datastore.Rollback(tx)
		ResponseSchema.RespCode = "02"
		ResponseSchema.Remak = err.Error()
		return
	}

	a.datastore.Commit(tx)

	var Saldo float64 = DataRekening.Saldo + params.Nominal
	ResponseSchema.RespCode = "00"
	ResponseSchema.Remak = fmt.Sprintf("Transaksi berhasil. Saldo saat ini adalah %.2f", Saldo)
	TabungSchema.Saldo = Saldo
	return
}

func (a *TransaksiApplication) CreateTarik(ctx *fiber.Ctx, params data.CreateTarikSchema) (ResponseSchema data.ResponseSchema, TarikSchema data.CreateTarikAssessmentSchema, err error) {
	tx, err := a.datastore.Begin()
	if err != nil {
		a.log.Error(logrus.Fields{
			"err":    err,
			"params": params,
		}, nil, "log error")
		a.datastore.Rollback(tx)
		ResponseSchema.RespCode = "02"
		ResponseSchema.Remak = err.Error()
		return
	}

	DataRekening, err := a.datastore.GetRekening(tx, params.NoRekening)
	if err != nil {
		a.log.Error(logrus.Fields{
			"err":    err,
			"params": params.NoRekening,
			"result": fmt.Sprintf("%+v", DataRekening),
		}, nil, "log error")
		a.datastore.Rollback(tx)
		ResponseSchema.RespCode = "02"
		ResponseSchema.Remak = err.Error()
		return
	}

	if DataRekening.Saldo < params.Nominal {
		a.log.Warn(logrus.Fields{
			"saldo":        DataRekening.Saldo,
			"nilai_mutasi": params.Nominal,
		}, nil, "log warning")
		a.datastore.Rollback(tx)
		ResponseSchema.RespCode = "02"
		ResponseSchema.Remak = "Saldo rekening tidak cukup untuk melakukan transaksi tarik"
		TarikSchema.Saldo = DataRekening.Saldo
		return
	}

	TanggalTransaksi := time.Now().Format("2006-01-02 15:04:05")
	var dataMutasi = data.CreateMutasiSchema{
		TanggalTransaksi: fmt.Sprintf("%v", TanggalTransaksi),
		NoRekening:       params.NoRekening,
		JenisTransaksi:   "D",
		Nominal:          params.Nominal,
	}
	jsonData, err := json.Marshal(dataMutasi)
	if err != nil {
		a.log.Error(logrus.Fields{
			"err":    err,
			"params": params,
		}, nil, "log error")
		a.datastore.Rollback(tx)
		ResponseSchema.RespCode = "02"
		ResponseSchema.Remak = err.Error()
		return
	}

	jsonString := string(jsonData)

	err = a.rdb.Publish(a.ctxRedis, "mutasi-transaksi", jsonString).Err()
	if err != nil {
		a.log.Error(logrus.Fields{
			"err":    err,
			"params": params,
		}, nil, "log error")
		a.datastore.Rollback(tx)
		ResponseSchema.RespCode = "02"
		ResponseSchema.Remak = err.Error()
		return
	}

	a.datastore.Commit(tx)

	var Saldo float64 = DataRekening.Saldo - params.Nominal
	ResponseSchema.RespCode = "00"
	ResponseSchema.Remak = fmt.Sprintf("Transaksi berhasil. Saldo saat ini %.2f", Saldo)
	TarikSchema.Saldo = Saldo

	return
}
