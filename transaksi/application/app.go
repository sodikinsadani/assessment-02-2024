package application

import (
	"context"
	"fmt"
	"transaksi/logging"

	"github.com/go-redis/redis/v8"
)

type TransaksiApplication struct {
	datastore TransaksiDatastorePort
	log       *logging.Logger
	ctxRedis  context.Context
	rdb       *redis.Client
	mySecret  string
}

func InitApplication(datastore TransaksiDatastorePort, log *logging.Logger, RedisHost string, RedistPort string, MySecret string) *TransaksiApplication {
	ctxRedis := context.Background()

	// Create a Redis client
	rdb := redis.NewClient(&redis.Options{
		Addr: fmt.Sprintf("%v:%v", RedisHost, RedistPort),
	})

	return &TransaksiApplication{
		datastore: datastore,
		log:       log,
		ctxRedis:  ctxRedis,
		rdb:       rdb,
		mySecret:  MySecret,
	}
}
