package main

import (
	"transaksi/api"
	"transaksi/application"
	"transaksi/logging"

	"transaksi/datastore"

	"github.com/spf13/viper"
)

func main() {
	viper.SetConfigName(".env")
	viper.SetConfigType("env")
	viper.AddConfigPath(".")
	viper.AutomaticEnv()
	err := viper.ReadInConfig()
	if err != nil {
		panic(err)
	}

	SERVICE := viper.GetString("SERVICE")
	SERVICE_PORT := viper.GetString("SERVICE_PORT")

	DbConfig := map[string]string{
		"db_driver":   viper.GetString("DB_DRIVER"),
		"db_user":     viper.GetString("DB_USER"),
		"db_password": viper.GetString("DB_PASSWORD"),
		"db_host":     viper.GetString("DB_HOST"),
		"db_port":     viper.GetString("DB_PORT"),
		"db_database": viper.GetString("DB_DATABASE"),
		"ssl_mode":    viper.GetString("SSL_MODE"),
		"time_zone":   viper.GetString("TIME_ZONE"),
	}

	DbSchemas := map[string]string{
		"public": viper.GetString("PUBLIC_SCHEMA"),
	}

	REDIS_HOST := viper.GetString("REDIS_HOST")
	REDIST_PORT := viper.GetString("REDIST_PORT")

	MY_SECRET := viper.GetString("MY_SECRET")

	logger := logging.NewLogger(SERVICE)
	ds := datastore.InitDatastore(logger, DbConfig, DbSchemas)
	application := application.InitApplication(ds, logger, REDIS_HOST, REDIST_PORT, MY_SECRET)
	api := api.InitTransaksiAPI(logger, application, SERVICE_PORT)
	api.Start()
}
