package datastore

import (
	"time"
	"transaksi/data"

	"github.com/sirupsen/logrus"
	"gorm.io/gorm"
)

func (a *TransaksiDatabase) GetRekening(tx *gorm.DB, nomorRekening string) (result data.Rekeningassessments, err error) {
	startTime := time.Now()

	sql := tx.Table("rekeningassessment").Take(&result, nomorRekening)

	err = sql.Error

	elapsedTime := time.Since(startTime)
	a.log.Info(logrus.Fields{"elapsedTime": elapsedTime, "sqlArg": nomorRekening}, nil, "log info")
	if err != nil {
		a.log.Error(
			logrus.Fields{
				"error": err,
			}, nil, "sql select",
		)
	}
	return
}
