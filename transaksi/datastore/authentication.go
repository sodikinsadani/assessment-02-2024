package datastore

import (
	"time"
	"transaksi/data"

	"github.com/sirupsen/logrus"
	"gorm.io/gorm"
)

func (a *TransaksiDatabase) GetAuthentication(tx *gorm.DB, params data.Authentication) (err error) {
	startTime := time.Now()

	var result data.Rekeningassessments

	sql := tx.Table("rekeningassessment").Where("nomor_rekening = ? and pin >= ?", params.AccountNo, params.Pin).Take(&result)

	err = sql.Error

	elapsedTime := time.Since(startTime)
	a.log.Info(logrus.Fields{"elapsedTime": elapsedTime}, nil, "log info")
	if err != nil {
		a.log.Error(
			logrus.Fields{
				"error": err,
			}, nil, "sql select",
		)
	}
	return
}
