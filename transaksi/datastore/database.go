package datastore

import (
	"fmt"

	"transaksi/logging"

	"github.com/sirupsen/logrus"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
)

type TransaksiDatabase struct {
	db        *gorm.DB
	log       *logging.Logger
	dbSchemas map[string]string
}

func (f *TransaksiDatabase) Begin() (tx *gorm.DB, err error) {
	tx = f.db.Begin()
	return
}

func (f *TransaksiDatabase) Rollback(tx *gorm.DB) {
	err := tx.Rollback()
	if err != nil {
		f.log.Error(logrus.Fields{
			"error": err.Error,
		}, nil, "failed to rollback transaction")
	}
}

func (f *TransaksiDatabase) Commit(tx *gorm.DB) {
	err := tx.Commit().Error
	if err != nil {
		f.log.Error(logrus.Fields{
			"error": err,
		}, nil, "failed to commit transaction")
	}
}

func InitDatastore(log *logging.Logger, DbConfig map[string]string, DbSchemas map[string]string) *TransaksiDatabase {
	dsn := fmt.Sprintf(`host=%v user=%v password=%v dbname=%v port=%v sslmode=%v TimeZone=%v`, DbConfig["db_host"], DbConfig["db_user"], DbConfig["db_password"], DbConfig["db_database"], DbConfig["db_port"], DbConfig["ssl_mode"], DbConfig["time_zone"])
	db, err := gorm.Open(postgres.Open(dsn), &gorm.Config{})
	if err != nil {
		panic(err)
	}

	return &TransaksiDatabase{
		db:        db,
		log:       log,
		dbSchemas: DbSchemas,
	}
}
