package test

import (
	"bytes"
	"encoding/json"
	"fmt"
	"net/http"
	"net/http/httptest"
	"testing"

	"transaksi/api"

	"github.com/gofiber/fiber/v2"
)

func HttpRequestForTest(app *fiber.App, method, url string, reqBody any, reqString any) (code int, respBody map[string]any, err error) {
	var req *http.Request
	if reqBody != nil {
		var bodyJson = []byte("")
		bodyJson, err = json.Marshal(reqBody)
		if err != nil {
			return
		}
		req = httptest.NewRequest(method, url, bytes.NewReader(bodyJson))
		req.Header.Add("Content-Type", "application/json")
		req.Context()
	} else if reqString != "" {
		var jsonValue = []byte("")
		jsonValue, err = json.Marshal(reqString)
		if err != nil {
			return
		}
		req = httptest.NewRequest(method, url, bytes.NewBuffer(jsonValue))
		req.Header.Add("Content-Type", "application/json")
	} else {
		req = httptest.NewRequest(method, url, nil)
	}

	resp, err := app.Test(req, 1000)
	if err != nil {
		return
	}
	code = resp.StatusCode

	// If no body content, we're done
	if resp.ContentLength == 0 {
		return
	}
	bodyData := make([]byte, resp.ContentLength)
	_, _ = resp.Body.Read(bodyData)
	err = json.Unmarshal(bodyData, &respBody)
	return
}

func TestTabung(t *testing.T) {
	app := fiber.New()
	defer app.Shutdown()
	TransaksiAPI := api.TransaksiAPI{}
	app.Post("/tabung", TransaksiAPI.CreateTabung)
	payload := map[string]string{
		"no_rekening": "0010000001",
		// "nominal":     1000,
	}

	// var payload = data.CreateTabungSchema{
	// 	NoRekening: "0010000001",
	// }

	code, body, err := HttpRequestForTest(app, "POST", "http://127.0.0.1:1224/tabung", payload, "")
	fmt.Println(code, body, err)

	// assert.Nil(t, err)
	// assert.Equal(t, 200, code)
	// assert.NotEmpty(t, body["data"])
}
