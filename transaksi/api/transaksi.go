package api

import (
	"fmt"
	"time"

	"transaksi/data"

	"github.com/gofiber/fiber/v2"
	"github.com/sirupsen/logrus"
)

func (a *TransaksiAPI) CreateTabung(ctx *fiber.Ctx) error {
	startTime := time.Now()
	a.log.Info(logrus.Fields{
		"ctx": ctx,
	}, nil, "info ctx")

	var payload data.CreateTabungSchema

	err := ctx.BodyParser(&payload)
	if err != nil {
		a.log.Error(logrus.Fields{
			"error": err,
			"data":  fmt.Sprintf("%+v", payload),
		}, nil, err.Error())

		return ctx.Status(fiber.StatusBadRequest).JSON(fiber.Map{
			"resp_code": "02",
			"remak":     err.Error(),
		})
	}

	ResponseSchema, TabungSchema, err := a.app.CreateTabung(ctx, payload)
	elapsedTime := time.Since(startTime)
	a.log.Info(logrus.Fields{
		"response status": fmt.Sprintf("%+v", ResponseSchema),
		"response data":   fmt.Sprintf("%+v", TabungSchema),
		"err":             err,
		"params":          fmt.Sprintf("%+v", payload),
		"elapsedTime":     elapsedTime,
	}, nil, "log info")

	if err != nil {
		a.log.Error(logrus.Fields{
			"response status": fmt.Sprintf("%+v", ResponseSchema),
			"response data":   fmt.Sprintf("%+v", TabungSchema),
			"err":             err,
			"params":          fmt.Sprintf("%+v", payload),
		}, nil, "log error")

		return ctx.Status(fiber.StatusBadRequest).JSON(fiber.Map{
			"resp_code": ResponseSchema.RespCode,
			"remak":     ResponseSchema.Remak,
		})
	}

	return ctx.Status(200).JSON(fiber.Map{
		"resp_code": ResponseSchema.RespCode,
		"remak":     ResponseSchema.Remak,
		"data":      TabungSchema,
	})
}

func (a *TransaksiAPI) CreateTarik(ctx *fiber.Ctx) error {
	startTime := time.Now()
	a.log.Info(logrus.Fields{
		"ctx": ctx,
	}, nil, "info ctx")

	var payload data.CreateTarikSchema

	err := ctx.BodyParser(&payload)
	if err != nil {
		a.log.Error(logrus.Fields{
			"error": err,
			"data":  fmt.Sprintf("%+v", payload),
		}, nil, err.Error())

		return ctx.Status(fiber.StatusBadRequest).JSON(fiber.Map{
			"resp_code": "02",
			"remak":     err.Error(),
		})
	}

	ResponseSchema, TarikSchema, err := a.app.CreateTarik(ctx, payload)
	elapsedTime := time.Since(startTime)
	a.log.Info(logrus.Fields{
		"response status": fmt.Sprintf("%+v", ResponseSchema),
		"response data":   fmt.Sprintf("%+v", TarikSchema),
		"err":             err,
		"params":          fmt.Sprintf("%+v", payload),
		"elapsedTime":     elapsedTime,
	}, nil, "log info")

	if err != nil {
		a.log.Error(logrus.Fields{
			"response status": fmt.Sprintf("%+v", ResponseSchema),
			"response data":   fmt.Sprintf("%+v", TarikSchema),
			"err":             err,
			"params":          fmt.Sprintf("%+v", payload),
		}, nil, "log error")

		return ctx.Status(fiber.StatusBadRequest).JSON(fiber.Map{
			"resp_code": ResponseSchema.RespCode,
			"remak":     ResponseSchema.Remak,
		})
	}

	return ctx.Status(200).JSON(fiber.Map{
		"resp_code": ResponseSchema.RespCode,
		"remak":     ResponseSchema.Remak,
		"data":      TarikSchema,
	})
}
