package api

import (
	"transaksi/data"

	"github.com/gofiber/fiber/v2"
	"github.com/sirupsen/logrus"
)

func (a *TransaksiAPI) Middleware(ctx *fiber.Ctx) (err error) {
	AuthenticationData := data.Authentication{
		AccountNo: ctx.Get("account_no"),
		Pin:       ctx.Get("pin"),
	}
	err = a.app.Authentication(ctx, AuthenticationData)
	if err != nil {
		a.log.Error(logrus.Fields{
			"err": err,
		}, nil, "log error")

		return ctx.Status(fiber.StatusUnauthorized).JSON(fiber.Map{
			"message": err.Error(),
		})
	}

	return ctx.Next()
}
