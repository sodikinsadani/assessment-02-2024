package api

import (
	"fmt"

	"transaksi/application"
	"transaksi/logging"

	"github.com/gofiber/fiber/v2"
)

type TransaksiAPI struct {
	app          application.TransaksiAppPort
	log          *logging.Logger
	service_port string
}

func (a *TransaksiAPI) Start() {
	app := fiber.New()

	app.Route("", func(router fiber.Router) {
		router.Post("/tabung", a.Middleware, a.CreateTabung)
		router.Post("/tarik", a.Middleware, a.CreateTarik)
	})

	app.Use(func(c *fiber.Ctx) error {
		return c.SendStatus(404)
	})

	app.Listen(fmt.Sprintf(`:%s`, a.service_port))
}

func InitTransaksiAPI(log *logging.Logger, app application.TransaksiAppPort, port string) *TransaksiAPI {
	return &TransaksiAPI{
		log:          log,
		app:          app,
		service_port: port,
	}
}
