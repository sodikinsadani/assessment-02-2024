package data

type CreateTabungSchema struct {
	NoRekening string  `db:"NO_REKENING" json:"no_rekening"`
	Nominal    float64 `db:"NOMINAL" json:"nominal"`
}

type CreateTabungAssessmentSchema struct {
	Saldo float64 `json:"saldo"`
}

type CreateTarikSchema struct {
	NoRekening string  `db:"NO_REKENING" json:"no_rekening"`
	Nominal    float64 `db:"NOMINAL" json:"nominal"`
}

type CreateTarikAssessmentSchema struct {
	Saldo float64 `json:"saldo"`
}

type CreateMutasiSchema struct {
	TanggalTransaksi string  `json:"tanggal_transaksi"`
	NoRekening       string  `json:"no_rekening"`
	JenisTransaksi   string  `json:"jenis_transaksi"`
	Nominal          float64 `json:"nominal"`
}
