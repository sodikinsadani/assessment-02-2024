package data

type Authentication struct {
	AccountNo string `json:"account_no"`
	Pin       string `json:"pin"`
}
