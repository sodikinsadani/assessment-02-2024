package data

type Rekeningassessments struct {
	NomorRekening string
	NamaRekening  string
	Nik           string
	NoHp          string
	Saldo         float64
	Pin           string
}
