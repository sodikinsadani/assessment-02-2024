package api

import (
	"time"

	"github.com/sirupsen/logrus"
)

func (a *MutasiAPI) CreateMutasiTrx(params string) (err error) {
	startTime := time.Now()

	err = a.app.CreateMutasiTrx(params)
	elapsedTime := time.Since(startTime)
	a.log.Info(logrus.Fields{
		"err":         err,
		"params":      params,
		"elapsedTime": elapsedTime,
	}, nil, "log info")

	if err != nil {
		a.log.Error(logrus.Fields{
			"err":    err,
			"params": params,
		}, nil, "log error")
	}

	return
}
