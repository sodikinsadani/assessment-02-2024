package api

import (
	"context"
	"fmt"
	"mutasi/application"
	"mutasi/logging"

	"github.com/go-redis/redis/v8"
	"github.com/sirupsen/logrus"
)

type MutasiAPI struct {
	log        *logging.Logger
	app        application.MutasiAppPort
	redisHost  string
	redistPort string
}

func (a *MutasiAPI) Start() {
	ctx := context.Background()

	// Create a Redis client
	rdb := redis.NewClient(&redis.Options{
		Addr: fmt.Sprintf("%v:%v", a.redisHost, a.redistPort),
	})

	// Subscribe to the channel "mychannel"
	pubsub := rdb.Subscribe(ctx, "mutasi-transaksi")
	defer pubsub.Close() // Remember to close the subscription

	a.log.Info(logrus.Fields{
		"pubsub": fmt.Sprintf("%+v", pubsub),
	}, nil, "service mutasi ready")

	// Listen for messages on the channel
	for {
		msg, err := pubsub.ReceiveMessage(ctx)
		if err != nil {
			a.log.Error(logrus.Fields{
				"msg": fmt.Sprintf("%+v", msg),
				"err": err,
			}, nil, "log error")
		} else {
			a.log.Info(logrus.Fields{
				"msg":     fmt.Sprintf("%+v", msg),
				"err":     err,
				"channel": msg.Channel,
				"Payload": msg.Payload,
			}, nil, "log info")
		}

		a.CreateMutasiTrx(msg.Payload)
	}
}

func InitMutasiAPI(log *logging.Logger, app application.MutasiAppPort, RedisHost string, RedistPort string) *MutasiAPI {
	return &MutasiAPI{
		log:        log,
		app:        app,
		redisHost:  RedisHost,
		redistPort: RedistPort,
	}
}
