package datastore

import (
	"mutasi/data"
	"time"

	"github.com/sirupsen/logrus"
	"gorm.io/gorm"
)

func (a *MutasiDatabase) CreateMutasiTrx(tx *gorm.DB, params data.Journalassessment) (err error) {
	startTime := time.Now()

	sql := tx.Table("journalassessment").Create(&params)

	err = sql.Error

	elapsedTime := time.Since(startTime)
	a.log.Info(logrus.Fields{"elapsedTime": elapsedTime, "sqlArg": params}, nil, "log info")
	if err != nil {
		a.log.Error(
			logrus.Fields{
				"error": err,
			}, nil, "sql insert",
		)
	}

	return
}

func (a *MutasiDatabase) UpdateSaldoByTrx(tx *gorm.DB, params data.UpdateSaldoTrxSchema) (err error) {
	startTime := time.Now()

	sql := tx.Table("rekeningassessment").Where("nomor_rekening = ?", params.NoRekening).Update("saldo", gorm.Expr("saldo + ?", params.Nominal))

	err = sql.Error

	elapsedTime := time.Since(startTime)
	a.log.Info(logrus.Fields{"elapsedTime": elapsedTime, "sqlArg": params}, nil, "log info")
	if err != nil {
		a.log.Error(
			logrus.Fields{
				"error": err,
			}, nil, "sql update",
		)
	}

	return
}
