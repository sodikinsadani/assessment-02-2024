package data

import "time"

type CreateMutasiSchema struct {
	TanggalTransaksi string  `json:"tanggal_transaksi"`
	NoRekening       string  `json:"no_rekening"`
	JenisTransaksi   string  `json:"jenis_transaksi"`
	Nominal          float64 `json:"nominal"`
}

type Journalassessment struct {
	TanggalTransaksi    time.Time
	NomorRekeningKredit string
	NomorRekeningDebit  string
	NominalKredit       float64
	NominalDebit        float64
}

type UpdateSaldoTrxSchema struct {
	NoRekening string  `db:"NO_REKENING" json:"no_rekening"`
	Nominal    float64 `db:"NOMINAL" json:"nominal"`
}

type Rekeningassessments struct {
	NomorRekening string
	NamaRekening  string
	Nik           string
	NoHp          string
	Saldo         float64
	Pin           string
}
