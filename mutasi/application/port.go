package application

import (
	"mutasi/data"

	"gorm.io/gorm"
)

type MutasiAppPort interface {
	CreateMutasiTrx(params string) (err error)
}

type MutasiDatastorePort interface {
	Begin() (tx *gorm.DB, err error)
	Rollback(tx *gorm.DB)
	Commit(tx *gorm.DB)

	CreateMutasiTrx(tx *gorm.DB, params data.Journalassessment) (err error)
	UpdateSaldoByTrx(tx *gorm.DB, params data.UpdateSaldoTrxSchema) (err error)
}
