package application

import "mutasi/logging"

type MutasiApplication struct {
	datastore MutasiDatastorePort
	log       *logging.Logger
}

func InitApplication(datastore MutasiDatastorePort, log *logging.Logger) *MutasiApplication {
	return &MutasiApplication{
		datastore: datastore,
		log:       log,
	}
}
