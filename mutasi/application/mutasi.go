package application

import (
	"encoding/json"
	"fmt"
	"mutasi/data"
	"time"

	"github.com/sirupsen/logrus"
)

func (a *MutasiApplication) CreateMutasiTrx(params string) (err error) {
	tx, err := a.datastore.Begin()
	if err != nil {
		a.log.Error(logrus.Fields{
			"err":    err,
			"params": params,
		}, nil, "log error")
		a.datastore.Rollback(tx)
		return
	}

	var dataMutasi = data.CreateMutasiSchema{}
	err = json.Unmarshal([]byte(params), &dataMutasi)
	if err != nil {
		a.log.Error(logrus.Fields{
			"err":        err,
			"params":     params,
			"dataMutasi": fmt.Sprintf("%+v", dataMutasi),
		}, nil, "log error")
		a.datastore.Rollback(tx)
		return
	}

	var TanggalTransaksi time.Time
	TanggalTransaksi, _ = time.Parse("2006-01-02 15:04:05", dataMutasi.TanggalTransaksi)

	var Journalassessment = data.Journalassessment{
		TanggalTransaksi:    TanggalTransaksi,
		NomorRekeningKredit: map[string]string{"D": "", "C": dataMutasi.NoRekening}[dataMutasi.JenisTransaksi],
		NomorRekeningDebit:  map[string]string{"D": dataMutasi.NoRekening, "C": ""}[dataMutasi.JenisTransaksi],
		NominalKredit:       map[string]float64{"D": 0, "C": dataMutasi.Nominal}[dataMutasi.JenisTransaksi],
		NominalDebit:        map[string]float64{"D": dataMutasi.Nominal, "C": 0}[dataMutasi.JenisTransaksi],
	}
	err = a.datastore.CreateMutasiTrx(tx, Journalassessment)
	if err != nil {
		a.log.Error(logrus.Fields{
			"err":    err,
			"params": fmt.Sprintf("%+v", Journalassessment),
		}, nil, "log error")
		a.datastore.Rollback(tx)
		return
	}

	var UpdateSaldoTrxSchema = data.UpdateSaldoTrxSchema{
		NoRekening: dataMutasi.NoRekening,
		Nominal:    map[string]float64{"D": -1 * dataMutasi.Nominal, "C": dataMutasi.Nominal}[dataMutasi.JenisTransaksi],
	}
	err = a.datastore.UpdateSaldoByTrx(tx, UpdateSaldoTrxSchema)
	if err != nil {
		a.log.Error(logrus.Fields{
			"err":    err,
			"params": fmt.Sprintf("%+v", UpdateSaldoTrxSchema),
		}, nil, "log error")
		a.datastore.Rollback(tx)
		return
	}

	a.datastore.Commit(tx)
	return
}
