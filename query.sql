-- public.rekeningassessment definition

-- Drop table

-- DROP TABLE public.rekeningassessment;

CREATE TABLE public.rekeningassessment (
	nomor_rekening varchar(20) NOT NULL,
	nama_rekening varchar(50) NULL,
	nik varchar(16) NULL,
	no_hp varchar(15) NULL,
	saldo numeric(30, 4) NULL,
	pin varchar(100) NULL,
	CONSTRAINT rekeningassessment_pkey PRIMARY KEY (nomor_rekening)
);

-- public.journalassessment definition

-- Drop table

-- DROP TABLE public.journalassessment;

CREATE TABLE public.journalassessment (
	id_journal bigserial NOT NULL,
	tanggal_transaksi timestamp(6) NULL,
	nomor_rekening_kredit varchar(50) NULL,
	nomor_rekening_debit varchar(50) NULL,
	nominal_kredit numeric(30, 4) NULL,
	nominal_debit numeric(30, 4) NULL,
	CONSTRAINT journalassessment_pkey PRIMARY KEY (id_journal)
);

INSERT INTO public.rekeningassessment
(nomor_rekening, nama_rekening, nik, no_hp, saldo, pin)
VALUES('0010000001', 'agus suherman', '', '', 28000.0000, 'WnIUt10a');